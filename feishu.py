# -*- coding: utf-8 -*-
# (C) 2014-2015, Matt Martz <matt@sivel.net>
# (C) 2017 Ansible Project
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)
from ansible.plugins.callback import CallbackBase
from ansible.module_utils.urls import open_url
from ansible.module_utils.common.text.converters import to_text
from ansible import context
import uuid
import os
import json
__metaclass__ = type

DOCUMENTATION = '''
    author: CaseyCui
    name: feishu
    type: notification
    requirements:
      - whitelist in configuration
      - prettytable (python library)
    short_description: Sends play events to a Feishu robot
    description:
        - This is an ansible callback plugin that sends status updates to a Feishu robot during playbook execution.
        - Before 2.4 only environment variables were available for configuring this plugin
    options:
      webhook_url:
        required: True
        description: Feishu Webhook URL
        env:
          - name: FEISHU_WEBHOOK_URL
        ini:
          - section: callback_feishu
            key: webhook_url
'''


try:
    import prettytable
    HAS_PRETTYTABLE = True
except ImportError:
    HAS_PRETTYTABLE = False


class CallbackModule(CallbackBase):
    """This is an ansible callback plugin that sends status
    updates to a Feishu robot during playbook execution.
    """
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'notification'
    CALLBACK_NAME = 'community.general.feishu'
    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self, display=None):

        super(CallbackModule, self).__init__(display=display)

        if not HAS_PRETTYTABLE:
            self.disabled = True
            self._display.warning('The `prettytable` python module is not '
                                  'installed. Disabling the Feishu callback '
                                  'plugin.')

        self.playbook_name = None

        # This is a 6 character identifier provided with each message
        # This makes it easier to correlate messages when there are more
        # than 1 simultaneous playbooks running
        self.guid = uuid.uuid4().hex[:6]

    def set_options(self, task_keys=None, var_options=None, direct=None):

        super(CallbackModule, self).set_options(
            task_keys=task_keys, var_options=var_options, direct=direct)

        self.webhook_url = self.get_option('webhook_url')
        # 之前是需要 -vv 才能显示, 现在是无论如何都显示
        self.show_invocation = (self._display.verbosity >= 0)

        if self.webhook_url is None:
            self.disabled = True
            self._display.warning('Feishu Webhook URL was not provided. The '
                                  'Feishu Webhook URL can be provided using '
                                  'the `FEISHU_WEBHOOK_URL` environment '
                                  'variable.')

    def send_msg(self, attachments):
        headers = {
            'Content-type': 'application/json',
        }
        payload = {
            "msg_type": "post",
            "content": {
                "post": {
                    "zh_cn": attachments
                }
            }
        }

        data = json.dumps(payload)
        self._display.debug(data)
        self._display.debug(self.webhook_url)
        try:
            response = open_url(self.webhook_url, data=data, headers=headers)
            return response.read()
        except Exception as e:
            self._display.warning(u'Could not submit message to Feishu: %s' %
                                  to_text(e))

    def v2_playbook_on_start(self, playbook):
        self.playbook_name = os.path.basename(playbook._file_name)

        # *Playbook initiated* (_8c0974_)
        title = '*🎬 Playbook 开始执行* (_%s_)' % self.guid
        text_items = []

        invocation_items = []
        if context.CLIARGS and self.show_invocation:
            tags = context.CLIARGS['tags']
            skip_tags = context.CLIARGS['skip_tags']
            extra_vars = context.CLIARGS['extra_vars']
            subset = context.CLIARGS['subset']
            inventory = [os.path.abspath(i)
                         for i in context.CLIARGS['inventory']]

            invocation_items.append('🖥️ Inventory:  %s' % ', '.join(inventory))
            if tags and tags != ['all']:
                invocation_items.append('🏷️ Tags:       %s' % ', '.join(tags))
            if skip_tags:
                invocation_items.append('🏷️ 跳过 Tags:  %s' %
                                        ', '.join(skip_tags))
            if subset:
                invocation_items.append('🖥️ 限定主机:      %s' % subset)
            if extra_vars:
                invocation_items.append('💲 Extra Vars: %s' %
                                        ' '.join(extra_vars))

            title += 'by *%s*' % context.CLIARGS['remote_user']

        text_items.append('*%s*' % self.playbook_name)
        msg_items = [' '.join(text_items)]
        if invocation_items:
            msg_items.append('```\n%s\n```' % '\n'.join(invocation_items))

        msg = '\n'.join(msg_items)

        attachments = {
            "title": title,
            "content": [[
                {
                    "tag": "text",
                    "text": msg
                }
            ]]
        }

        self.send_msg(attachments=attachments)

    def v2_playbook_on_play_start(self, play):
        """Display Play start messages"""

        name = play.name or 'Play name not specified (%s)' % play._uuid
        msg = '*🎞️ 执行 play* (_%s_)' % self.guid
        attachments = {
            "title": msg,
            "content": [[
                {
                    "tag": "text",
                    "text": name
                }
            ]]
        }
        self.send_msg(attachments=attachments)

    def v2_playbook_on_stats(self, stats):
        """Display info about playbook statistics"""

        hosts = sorted(stats.processed.keys())

        t = prettytable.PrettyTable(['主机(Host)', '成功(Ok)', '已改变(Changed)', '不可达(Unreachable)',
                                     '失败(Failures)', '被救回(Rescued)', '已忽略(Ignored)'])

        failures = False
        unreachable = False

        for h in hosts:
            s = stats.summarize(h)

            if s['failures'] > 0:
                failures = True
            if s['unreachable'] > 0:
                unreachable = True

            t.add_row([h] + [s[k] for k in ['ok', 'changed', 'unreachable',
                                            'failures', 'rescued', 'ignored']])

        # attachments = []
        title = '*📽️ Playbook 执行完毕* (_%s_)' % self.guid
        content = ''

        if failures or unreachable:
            color = 'danger'
            content += '*❌ 失败!*'
        else:
            color = 'good'
            content += '*✔️ 成功!*'

        content += '\n```\n%s\n```' % t

        attachments = {
            "title": title,
            "content": [[
                {
                    "tag": "text",
                    "text": content
                }
            ]]
        }

        self.send_msg(attachments=attachments)
