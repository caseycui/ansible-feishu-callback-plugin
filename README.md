# Ansible Feishu Callback Plugin

回调插件响应Ansible发送的事件，可以用来通知外部系统。

本插件用途, 当Ansible playbook执行完毕时，通知 Feishu。

## TODO:

- [x] Title 优化
- [x] 加入 Emoji
- [x] Inventory, Tags, Limit, Extra Vars 可以正常显示 
- [ ] 表格优化

## 先决条件

对于Feishu插件的工作，有三个要求需要满足，一个是需要安装python库，另一个是需要在Feishu 创建一个 robot。

### 将 `feishu.py` 放入指定目录 -  `ansible/plugins/callback`

如对于 ubuntu 20.04, 该目录是: `/usr/lib/python3/dist-packages/ansible/plugins/callback`

### Python Prettytable

[Prettytable](https://pypi.org/project/PrettyTable/) 是一个用于生成简单ASCII表格的Python库。它可以用pip安装，使用 `pip3 install prettytable`

### Feishu Webhooks

为你的 Feishu 创建Webhooks是非常简单的。你需要在ansible设置中保留webhook的URL，以便使用。

## 配置

一旦你设置了先决条件，还有一些事情需要做。

`ansible.cfg`中的一些东西需要被配置。

```
[defaults]
callback_whitelist = feishu

[callback_feishu]
webhook_url = https://open.feishu.cn/open-apis/bot/v2/hook/<your-id>
```

webhook_url参数将是Feishu提供的用于发送消息的网址。

请注意!如果你打算在生产中使用，你也可以将这些数据存储在环境变量中，请参见相关文档。

## 运行

当你下次运行一个playbook 时，你会看到信息进入选定的通道。

运行 ansible-playbook, 如:

```bash
ansible-playbook change_registries_yaml.yml -i inventory/my-cluster/hosts.ini
```

Feishu 上的显示如下:

![](https://pic-cdn.ewhisper.cn/img/2022/03/17/5a32b77d0cda8763a686ef1ff9da94ce-3a8865c205d5dd97693330f6c4a6b21.png)
